# fantube

Device that regulates fan speed and damper in the tube depending on temperature

![fantube](https://dev.ussr.win/microcontrollers/fantube/raw/branch/master/fantube_agg.jpg)

![fantube](https://dev.ussr.win/microcontrollers/fantube/raw/branch/master/fantube_bb.jpg)

